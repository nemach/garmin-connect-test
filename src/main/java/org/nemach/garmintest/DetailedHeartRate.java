package org.nemach.garmintest;

import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DetailedHeartRate {
	
	//	{
	//		"userProfilePK": 46720859,
	//		"calendarDate": "2019-05-25",
	//		"startTimestampGMT": "2019-05-24T22:00:00.0",
	//		"endTimestampGMT": "2019-05-25T22:00:00.0",
	//		"startTimestampLocal": "2019-05-25T00:00:00.0",
	//		"endTimestampLocal": "2019-05-26T00:00:00.0",
	//		"maxHeartRate": 131,
	//		"minHeartRate": 44,
	//		"restingHeartRate": 51,
	//		"lastSevenDaysAvgRestingHeartRate": 51,
	//		"heartRateValues": [
	//			[
	//				1558735200000,
	//				55
	//			],
	//			[
	//				1558735320000,
	//				55
	//			]
	//		],
	//		"heartRateValueDescriptors": [
	//			{
	//				"key": "timestamp",
	//				"index": 0
	//			},
	//			{
	//				"key": "heartrate",
	//				"index": 1
	//			}
	//		]
	//	}
	
	private Integer userProfilePK;
	private String calendarDate;
	private String startTimestampGMT;
	private String endTimestampGMT;
	private String startTimestampLocal;
	private String endTimestampLocal;
	private Integer maxHeartRate;
	private Integer minHeartRate;
	private Integer restingHeartRate;
	private Integer lastSevenDaysAvgRestingHeartRate;
	private List<List<Long>> heartRateValues;
	private List<HeartRateDescriptor> heartRateValueDescriptors;
	/**
	 * @return the userProfilePK
	 */
	public Integer getUserProfilePK() {
		return userProfilePK;
	}
	/**
	 * @param userProfilePK the userProfilePK to set
	 */
	public void setUserProfilePK(Integer userProfilePK) {
		this.userProfilePK = userProfilePK;
	}
	/**
	 * @return the calendarDate
	 */
	public String getCalendarDate() {
		return calendarDate;
	}
	/**
	 * @param calendarDate the calendarDate to set
	 */
	public void setCalendarDate(String calendarDate) {
		this.calendarDate = calendarDate;
	}
	/**
	 * @return the startTimestampGMT
	 */
	public String getStartTimestampGMT() {
		return startTimestampGMT;
	}
	/**
	 * @param startTimestampGMT the startTimestampGMT to set
	 */
	public void setStartTimestampGMT(String startTimestampGMT) {
		this.startTimestampGMT = startTimestampGMT;
	}
	/**
	 * @return the endTimestampGMT
	 */
	public String getEndTimestampGMT() {
		return endTimestampGMT;
	}
	/**
	 * @param endTimestampGMT the endTimestampGMT to set
	 */
	public void setEndTimestampGMT(String endTimestampGMT) {
		this.endTimestampGMT = endTimestampGMT;
	}
	/**
	 * @return the startTimestampLocal
	 */
	public String getStartTimestampLocal() {
		return startTimestampLocal;
	}
	/**
	 * @param startTimestampLocal the startTimestampLocal to set
	 */
	public void setStartTimestampLocal(String startTimestampLocal) {
		this.startTimestampLocal = startTimestampLocal;
	}
	/**
	 * @return the endTimestampLocal
	 */
	public String getEndTimestampLocal() {
		return endTimestampLocal;
	}
	/**
	 * @param endTimestampLocal the endTimestampLocal to set
	 */
	public void setEndTimestampLocal(String endTimestampLocal) {
		this.endTimestampLocal = endTimestampLocal;
	}
	/**
	 * @return the maxHeartRate
	 */
	public Integer getMaxHeartRate() {
		return maxHeartRate;
	}
	/**
	 * @param maxHeartRate the maxHeartRate to set
	 */
	public void setMaxHeartRate(Integer maxHeartRate) {
		this.maxHeartRate = maxHeartRate;
	}
	/**
	 * @return the minHeartRate
	 */
	public Integer getMinHeartRate() {
		return minHeartRate;
	}
	/**
	 * @param minHeartRate the minHeartRate to set
	 */
	public void setMinHeartRate(Integer minHeartRate) {
		this.minHeartRate = minHeartRate;
	}
	/**
	 * @return the restingHeartRate
	 */
	public Integer getRestingHeartRate() {
		return restingHeartRate;
	}
	/**
	 * @param restingHeartRate the restingHeartRate to set
	 */
	public void setRestingHeartRate(Integer restingHeartRate) {
		this.restingHeartRate = restingHeartRate;
	}
	/**
	 * @return the lastSevenDaysAvgRestingHeartRate
	 */
	public Integer getLastSevenDaysAvgRestingHeartRate() {
		return lastSevenDaysAvgRestingHeartRate;
	}
	/**
	 * @param lastSevenDaysAvgRestingHeartRate the lastSevenDaysAvgRestingHeartRate to set
	 */
	public void setLastSevenDaysAvgRestingHeartRate(Integer lastSevenDaysAvgRestingHeartRate) {
		this.lastSevenDaysAvgRestingHeartRate = lastSevenDaysAvgRestingHeartRate;
	}
	/**
	 * @return the heartRateValues
	 */
	public List<List<Long>> getHeartRateValues() {
		return heartRateValues;
	}
	/**
	 * @param heartRateValues the heartRateValues to set
	 */
	public void setHeartRateValues(List<List<Long>> heartRateValues) {
		this.heartRateValues = heartRateValues;
	}
	/**
	 * @return the heartRateValueDescriptors
	 */
	public List<HeartRateDescriptor> getHeartRateValueDescriptors() {
		return heartRateValueDescriptors;
	}
	/**
	 * @param heartRateValueDescriptors the heartRateValueDescriptors to set
	 */
	public void setHeartRateValueDescriptors(List<HeartRateDescriptor> heartRateValueDescriptors) {
		this.heartRateValueDescriptors = heartRateValueDescriptors;
	}

	private Integer getIndexFromDescriptor(String key) {
		Integer index = null;
		if (null != heartRateValueDescriptors) {
			Iterator<HeartRateDescriptor> it = heartRateValueDescriptors.iterator();
			do {
				HeartRateDescriptor des = it.next();
				if (key.equals(des.getKey())) {
					index = des.getIndex();
				}
			} while(it.hasNext() && index == null);
		}
		return index;
	}
	
	public Map<Date, Integer> getHeartrateSamples() {
		TreeMap<Date, Integer> heartrateSamples = new TreeMap<>();
		Integer dateIndex = getIndexFromDescriptor("timestamp");
		Integer hrIndex = getIndexFromDescriptor("heartrate");
		if (null != heartRateValues && null != dateIndex && null != hrIndex) {
			heartRateValues.forEach( pair -> {
				System.out.println(pair.toString());
				if (null != pair.get(hrIndex) && null != pair.get(dateIndex)) {
					Date date = Date.from( Instant.ofEpochSecond(pair.get(dateIndex)/1000));
					heartrateSamples.put(date, pair.get(hrIndex).intValue());
				}
			});
		}
		return heartrateSamples;
	}
	
}