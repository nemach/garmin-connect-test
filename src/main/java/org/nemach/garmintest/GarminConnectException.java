package org.nemach.garmintest;

public class GarminConnectException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5615748225276450253L;

	public GarminConnectException(String message) {
        super(message);
    }

    public GarminConnectException(String message, Throwable cause) {
        super(message, cause);
    }
}