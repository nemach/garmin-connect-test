package org.nemach.garmintest;

public class HeartRateDescriptor {

	//		"heartRateValueDescriptors": [
	//			{
	//				"key": "timestamp",
	//				"index": 0
	//			},
	//			{
	//				"key": "heartrate",
	//				"index": 1
	//			}
	//		]
	
	private String key;
	private Integer index;
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}
	/**
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	
}
