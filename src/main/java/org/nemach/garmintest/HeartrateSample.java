package org.nemach.garmintest;

import java.util.Date;

public class HeartrateSample {
	private Integer hr;
	private Date time;
	
	/**
	 * @return the hr
	 */
	public Integer getHr() {
		return hr;
	}
	/**
	 * @param hr the hr to set
	 */
	public void setHr(Integer hr) {
		this.hr = hr;
	}
	/**
	 * @return the time
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	
}
