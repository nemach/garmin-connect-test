/**
 * 
 */
package org.nemach.garmintest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author neil
 *
 */
public class GarminConnectTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		GarminConnectClient gc = new GarminConnectClient("neil@hydeabbey.net", "");

		try {
			gc.initialiseSession();
		}catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	
		// https://connect.garmin.com/modern/proxy/wellness-service/wellness/dailyHeartRate?date=2019-05-25
		// first data at 2017-06-22
		LocalDateTime startDate = LocalDateTime.of(2017, 06, 22, 22, 00);
		String dateString = startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")); 
		String nextHeartrate = gc.callService("https://connect.garmin.com/modern/proxy/wellness-service/wellness/dailyHeartRate?date=" + dateString);
		
//		String nextHeartrate = "{\r\n" + 
//				"	\"userProfilePK\": 46720859,\r\n" + 
//				"	\"calendarDate\": \"2019-05-25\",\r\n" + 
//				"	\"startTimestampGMT\": \"2019-05-24T22:00:00.0\",\r\n" + 
//				"	\"endTimestampGMT\": \"2019-05-25T22:00:00.0\",\r\n" + 
//				"	\"startTimestampLocal\": \"2019-05-25T00:00:00.0\",\r\n" + 
//				"	\"endTimestampLocal\": \"2019-05-26T00:00:00.0\",\r\n" + 
//				"	\"maxHeartRate\": 131,\r\n" + 
//				"	\"minHeartRate\": 44,\r\n" + 
//				"	\"restingHeartRate\": 51,\r\n" + 
//				"	\"lastSevenDaysAvgRestingHeartRate\": 51,\r\n" + 
//				"	\"heartRateValues\": [\r\n" + 
//				"		[\r\n" + 
//				"			1558735200000,\r\n" + 
//				"			55\r\n" + 
//				"		],\r\n" + 
//				"		[\r\n" + 
//				"			1558735320000,\r\n" + 
//				"			55\r\n" + 
//				"		]\r\n" + 
//				"\r\n" + 
//				"	],\r\n" + 
//				"	\"heartRateValueDescriptors\": [\r\n" + 
//				"		{\r\n" + 
//				"			\"key\": \"timestamp\",\r\n" + 
//				"			\"index\": 0\r\n" + 
//				"		},\r\n" + 
//				"		{\r\n" + 
//				"			\"key\": \"heartrate\",\r\n" + 
//				"			\"index\": 1\r\n" + 
//				"		}\r\n" + 
//				"	]\r\n" + 
//				"}";
//		System.out.println(nextHeartrate);
		
		ObjectMapper mapper = new ObjectMapper();
		DetailedHeartRate heartRate = null;
		try {
			heartRate = mapper.readValue(nextHeartrate, DetailedHeartRate.class);
		} catch (JsonProcessingException e) {
			System.out.println(e.toString());
			System.exit(2);
		}
		heartRate.getHeartrateSamples().forEach( (k, v) -> System.out.println(k.toString() + "=" + v.toString()));
		
	}

}
