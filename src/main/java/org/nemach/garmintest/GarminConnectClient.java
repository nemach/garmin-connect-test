package org.nemach.garmintest;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class GarminConnectClient {

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36";
	private static final String BASE_LOCATION_URL = "https://connect.garmin.com";
	private static final String AFTER_LOGIN_URL = "https://connect.garmin.com/modern";
	private static final String LOGIN_URL = "https://sso.garmin.com/sso/signin";

	private final String username;
	private final String password;

	private final ObjectMapper objectMapper;
	private final CloseableHttpClient httpClient;
	private final CookieStore cookieStore;
	private final HttpContext httpContext;

	private boolean authenticated = false;;

	public GarminConnectClient(String username, String password) {
		this.username = username;
		this.password = password;

		this.httpClient = HttpClients.custom().useSystemProperties().disableRedirectHandling().build();

		cookieStore = new BasicCookieStore();
		httpContext = new BasicHttpContext();
		httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

		this.objectMapper = new ObjectMapper();
		this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public String callService(String url) {
		if (!this.authenticated) {
			this.initialiseSession();
		}

		CloseableHttpResponse response = null;		
		try {
			HttpGet request = new HttpGet(url);

			response = this.executeRequest(request);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN) {
				this.authenticated = false;

				// reauthenticate
				this.initialiseSession();

				// try again
				response.close();
				response = this.executeRequest(request);
			}

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				throw new GarminConnectException("Unexpected response from server: " + response.getStatusLine());
			}

			HttpEntity entity = response.getEntity();
			Header encoding = entity.getContentEncoding();
			String enc = encoding == null ? "UTF-8" : encoding.getValue();
			String body = EntityUtils.toString(entity, enc);
			return body;
			//return response.getEntity().getContent().toString(); 
			
		} catch (IOException e) {
			throw new GarminConnectException("Could not get activities", e);
		} finally {
			if (null != response) {
				try {
					response.close();
				} catch (IOException e) {
				}
			}
		}
		
	}

	private CloseableHttpResponse executeRequest(HttpUriRequest request) throws IOException {
		request.setHeader("User-Agent", USER_AGENT);
		return this.httpClient.execute(request, httpContext);
	}

	public void initialiseSession() {

		// based on tapiriik: https://github.com/cpfair/tapiriik
		HashMap<String, String> data = new HashMap<>();
		data.put("username", username);
		data.put("password", password);
		data.put("_eventId", "submit");
		data.put("embed", "true");

		Map<String, String> params = new HashMap<>();
		params.put("service", "https://connect.garmin.com/modern");
		params.put("clientId", "GarminConnect");
		params.put("gauthHost", "https://sso.garmin.com/sso");
		params.put("consumeServiceTicket", "false");

		String garminSigninHeaderKey = "origin";
		String garminSigninHeaderValue = "https://sso.garmin.com";

		CloseableHttpResponse response = null;

		try {

			URIBuilder builder;
			String content;
			URI uri;

			builder = new URIBuilder(LOGIN_URL);
			params.forEach(builder::addParameter);
			uri = builder.build();

			HttpGet request = new HttpGet(uri);
			response = this.executeRequest(request);

//			content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
//			System.out.println(content);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				throw new GarminConnectException("SSO prestart error: " + response.getStatusLine());
			}

			response.close();

			builder = new URIBuilder(LOGIN_URL);
			params.forEach(builder::addParameter);
			uri = builder.build();
			HttpPost post = new HttpPost(uri);

			post.setHeader(garminSigninHeaderKey, garminSigninHeaderValue);
			ArrayList<NameValuePair> postData = new ArrayList<>();
			data.forEach((k, v) -> postData.add(new BasicNameValuePair(k, v)));
			post.setEntity(new UrlEncodedFormEntity(postData, "UTF-8"));
			response = this.executeRequest(post);

//        content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
//        System.out.println(content);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK
					|| response.getStatusLine().getReasonPhrase().contains("temporarily unavailable")) {
				throw new GarminConnectException("SSO error: " + response.getStatusLine());
			} else if (response.getStatusLine().getReasonPhrase().contains("FAIL")) {
				throw new GarminConnectException("Invalid login" + response.getStatusLine());
			} else if (response.getStatusLine().getReasonPhrase().contains("ACCOUNT_LOCKED")) {
				throw new GarminConnectException("Account locked" + response.getStatusLine());
			} else if (response.getStatusLine().getReasonPhrase().contains("renewPassword")) {
				throw new GarminConnectException("Reset password" + response.getStatusLine());
			}

			if (null != response) {
				response.close();
			}

			request = new HttpGet(AFTER_LOGIN_URL);
			response = this.executeRequest(request);

//        content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
//        System.out.println(content);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_MOVED_TEMPORARILY) {
				throw new GarminConnectException("GC redeem-start error : " + response.getStatusLine());
			}
			response.close();

			String url_prefix = BASE_LOCATION_URL;
			int max_redirect_count = 7;
			int current_redirect_count = 1;
			while (true) {
//            self._rate_limit()
				Header[] location = response.getHeaders("location");
				if (location.length < 1) {
					throw new GarminConnectException("Could not extract location from header!");
				}
				String url = location[0].getValue();
				if (url.startsWith("/")) {
					url = url_prefix + uri;
				}
				String[] split = url.split("/");
				if (split.length < 3) {
					throw new GarminConnectException("location does not contain valid URL: " + location[0]);
				}
				url_prefix = String.join("/", split[0], split[1], split[2]);
				request = new HttpGet(url);
				response.close();
				response = this.executeRequest(request);

//            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
//            System.out.println(content);

				if (current_redirect_count >= max_redirect_count
						&& response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new GarminConnectException("GC redeem error : " + response.getStatusLine());
				}

				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK
						|| response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND) {
					break;
				}
				current_redirect_count += 1;
				if (current_redirect_count > max_redirect_count) {
					break;
				}
			}

			request = new HttpGet("https://connect.garmin.com/modern/proxy/activitylist-service/activities/search/activities");
			response = this.executeRequest(request);

//			content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
//			System.out.println(content);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				throw new GarminConnectException("error: " + response.getStatusLine());
			}

		} catch (Exception e) {
			throw new GarminConnectException("Initialisation failed", e);
		} finally {
			if (null != response) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		authenticated = true;
		
	}
}